package com.pingwinno;

import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static final String SERVER_HOST = "localhost";
    private static final int SERVER_PORT = 2523;
    private org.slf4j.Logger log = LoggerFactory.getLogger(getClass().getName());
    private Socket clientSocket;
    private Scanner inputMessagesStream;
    private PrintWriter outputMessagesStream;
    private MessageHandler messageHandler = new MessageHandler();

    public Client() throws IOException {
        //initialize client
        log.info("initialize client");
        clientSocket = new Socket(SERVER_HOST, SERVER_PORT);
        inputMessagesStream = new Scanner(clientSocket.getInputStream());
        outputMessagesStream = new PrintWriter(clientSocket.getOutputStream());
        new Thread(() -> {
            log.debug("start incoming message loop");
            //start incoming message loop
            while (clientSocket != null) {
                if (inputMessagesStream.hasNext()) {
                    String inMes = inputMessagesStream.nextLine();
                    log.debug(messageHandler.parseMessage(inMes));
                }
            }
        }).start();
    }

    public void sendMessage(String message) {
        log.debug("sending message");
        outputMessagesStream.println(message);
        outputMessagesStream.flush();
    }

    public void close() throws IOException {
        clientSocket.close();
        outputMessagesStream.close();
        inputMessagesStream.close();
    }
}