package com.pingwinno;

import com.google.gson.Gson;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class MessageHandler {
    private Gson gson = new Gson();
    private MessageModel messageModel = new MessageModel();

    public void setUsername(String username) {
        messageModel.setUsername(username);
    }

    public String makeMessage(String message) {
        LocalDateTime timePoint = LocalDateTime.now();
        messageModel.setMessage(message);
        //add timestamp to message
        messageModel.setTime(timePoint);
        return gson.toJson(messageModel);
    }

    public String parseMessage(String message) {
        messageModel = gson.fromJson(message, MessageModel.class);

        return "[" + messageModel.getTime().format(DateTimeFormatter.ofPattern("dd MMM yyyy hh:mm:ss")) + "]"
                + " " + messageModel.getUsername() + ":" + messageModel.getMessage();
    }

}
