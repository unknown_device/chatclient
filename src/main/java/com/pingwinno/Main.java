package com.pingwinno;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Scanner;


public class Main {



    public static void main(String[] args) {

        Logger log = LoggerFactory.getLogger(Main.class.getName());

        MessageHandler handler = new MessageHandler();
        Scanner in = new Scanner(System.in);
        try {
            //get username
            log.debug("waiting for username...");
            log.info("Set username");
            String userName = "";
            boolean flag = true;
            while (flag) {
                userName = in.nextLine();
                if (userName.trim().isEmpty()) {
                    log.info("Username is empty. Please enter username");
                    continue;
                }
               flag = false;
            }
            handler.setUsername(userName);
            Client client = new Client();
            //parse user input loop
            log.debug("Connection established");
            log.info("Waiting for input...");
            while (true) {
                String message = in.nextLine();
                if (message == null || message.trim().isEmpty()){
                    log.info("Empty message not allowed");
                    continue;
                }
                client.sendMessage(handler.makeMessage(message));
                if (message.equals("!exit")){
                    log.debug("Closing client");
                    client.close();
                    System.exit(0);
                }

            }
        } catch (IOException e) {
           log.error("Client start failed {}", e.toString());
        }
    }
}
